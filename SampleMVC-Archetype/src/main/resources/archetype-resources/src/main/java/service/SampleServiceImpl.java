package service;

import model.SignupForm;
import org.springframework.stereotype.Service;

/**
 * User: ryan
 * Date: 2/8/13
 */
@Service
public class SampleServiceImpl implements SampleService {
    public Boolean saveFrom(SignupForm signupForm) {
        return !"Dave".equals(signupForm.getFirstName());
    }
}

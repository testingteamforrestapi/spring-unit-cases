package service;

import model.SignupForm;

/**
 * User: ryan
 * Date: 2/8/13
 */
public interface SampleService {

    public Boolean saveFrom(SignupForm signupForm);

}

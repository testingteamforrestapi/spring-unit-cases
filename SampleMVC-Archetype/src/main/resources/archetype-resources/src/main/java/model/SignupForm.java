package model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * User: ryan
 * Date: 2/7/13
 */
public class SignupForm {


    private String firstName;
    private String lastName;
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
